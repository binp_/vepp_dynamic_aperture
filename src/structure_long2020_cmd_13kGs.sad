MOMENTUM  = 1.0 GEV; 
ON ECHO; OFF CTIME;
!
!CONSTANTs in SI
R    = 1.40;    ! [m] - bend radius
HR   = 3.33563; ! [T*m]
Lmag = 1.09956; ! [m] - magnet length

km   = 0;       ! switch of nonlinear fringes of dipoles. ON/OFF = 0/1
kq   = 1;       ! quads fringe switch (DISFRIN)
kqf  = 0;       ! quads fringe setter (FRINGE)
ks   = 1;       ! solen fringe switch
kb0  = 0;       ! coef for dipole corrector

!#DRIFTS
DRIFT D1       = (L  = 48.223  / 100)
      D3       = (L  = 2.332   / 100)
      D5       = (L  = 20.5633 / 100)
      D7       = (L  = 16.0    / 100)
      D9       = (L  = 16.0    / 100)
      D11      = (L  = 5.8     / 100)
      D13      = (L  = 3.0     / 100)
      D15      = (L  = 5.0     / 100)
      D17      = (L  = 12.0    / 100)
      D19      = (L  = 18.0    / 100)
      D21      = (L  = 16.0    / 100)
      D23      = (L  = 5.0     / 100)
      D25      = (L  = 4.875   / 100)
      D27      = (L  = 50.821  / 100)
      Dsol100  = (L  = 47.0    / 100)
      D101     = (L  = 1.223   / 100)
      Dsol102  = (L  = 16.9    / 100)
      Dsol104  = (L  = 52.5823 / 100)
      Dsol202  = (L  = 16.9    / 100)
      Dsol204  = (L  = 52.5823 / 100)
      Dsol302  = (L  = 16.9    / 100)
      Dsol304  = (L  = 52.5823 / 100)
      Dsol402  = (L  = 16.9    / 100)
      Dsol404  = (L  = 52.5823 / 100)
;
!#SOLENOIDS
SOL   SOL100   = (BZ =  -13.4716 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL102   = (BZ =  -42.3837 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL104   = (BZ =   118.602 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL202   = (BZ =  -37.465  / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL204   = (BZ =  -118.789 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL302   = (BZ =  -37.465  / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL304   = (BZ =   118.789 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL402   = (BZ =   42.3837 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOL404   = (BZ =  -118.602 / 10 BOUND = 1 GEO = 1 DISFRIN=ks)
      SOLend   = (BZ =   0.0000       BOUND = 1         DISFRIN=ks)
;
!#BENDING MAGNETS
BEND  
      B        = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
                     
      B1M1     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B1M2     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B2M2     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B2M1     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B3M1     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B3M2     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B4M2     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
      B4M1     = (L  = Lmag  K0 = 0  K1 = 0.00673819 * 10 / HR * Lmag  ANGLE = Lmag/R  DISFRIN=km)
;
!#MULTIPOLES - example, not in use now
MULT  MVZQ1D2  =  (L = 14/100 K1 = -2.54313*0.641/0.7*10/HR*0.14  DISFRIN=kq  FRINGE=kqf) !; z-corr in quads
;
!#SEXTUPOLES - example, not in use now
SEXT  SZ      = (L  = 3.85/100 K2 = 0 * 1000 / HR * 0.0385  DISFRIN=1)
      SX      = (L  = 2/100    K2 = 0 * 1000 / HR * 0.02    DISFRIN=1)
;
!#QUADRUPOLES
QUAD  Q1F1    = (L  = 6/100  K1 =  1.17811 * 10 / HR * 0.06  DISFRIN=kq  FRINGE=kqf)
      Q1D1    = (L  = 14/100 K1 = -1.84755 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q1F2    = (L  = 19/100 K1 =  5.1875  * 10 / HR * 0.19  DISFRIN=kq  FRINGE=kqf)
      Q1D2    = (L  = 14/100 K1 = -3.19817 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q1D3    = (L  = 14/100 K1 = -4.58257 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q1F3    = (L  = 14/100 K1 =  4.88376 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
                     
      Q2F1    = (L  = 6/100  K1 =  1.17811 * 10 / HR * 0.06  DISFRIN=kq  FRINGE=kqf)
      Q2D1    = (L  = 14/100 K1 = -1.84755 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q2F2    = (L  = 19/100 K1 =  5.1875  * 10 / HR * 0.19  DISFRIN=kq  FRINGE=kqf)
      Q2D2    = (L  = 14/100 K1 = -3.19817 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q2D3    = (L  = 14/100 K1 = -4.58257 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q2F3    = (L  = 14/100 K1 =  4.88376 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)

      Q3F1    = (L  = 6/100  K1 =  1.17811 * 10 / HR * 0.06  DISFRIN=kq  FRINGE=kqf)
      Q3D1    = (L  = 14/100 K1 = -1.84755 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q3F2    = (L  = 19/100 K1 =  5.1875  * 10 / HR * 0.19  DISFRIN=kq  FRINGE=kqf)
      Q3D2    = (L  = 14/100 K1 = -3.19817 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q3D3    = (L  = 14/100 K1 = -4.58257 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q3F3    = (L  = 14/100 K1 =  4.88376 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)

      Q4F1    = (L  = 6/100  K1 =  1.17811 * 10 / HR * 0.06  DISFRIN=kq  FRINGE=kqf)
      Q4D1    = (L  = 14/100 K1 = -1.84755 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q4F2    = (L  = 19/100 K1 =  5.1875  * 10 / HR * 0.19  DISFRIN=kq  FRINGE=kqf)
      Q4D2    = (L  = 14/100 K1 = -3.19817 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q4D3    = (L  = 14/100 K1 = -4.58257 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
      Q4F3    = (L  = 14/100 K1 =  4.88376 * 10 / HR * 0.14  DISFRIN=kq  FRINGE=kqf)
;
!#RF CAVITY
CAVI  RF     = (L = 50.821*2/100 VOLT=0.06 MV HARM = 14)
;
MARK  IP1     = (DP = 0)
      IP2     = (DP = 0)
;
LINE CELL = (IP1, D1, SOL102, Dsol102, SOLend, D3, SOL104, Dsol104, SOLend);

FFS USE   = CELL;
V0VSND    = ExtractBeamLine[];

V1 = BeamLine[D5, Q1F1, D7, B1M1, D9, SZ, SZ, D11, Q1D1, D13, SX, SX, D15, Q1F2, D17, Q1D2, D19, B1M2, D21, SX, SX, D23, Q1D3, D25, Q1F3, D27];

V2 = BeamLine[D27, Q2F3, D25, Q2D3, D23, SX, SX, D21, B2M2, D19, Q2D2, D17, Q2F2, D15, SX, SX, D13, Q2D1, D11, SZ, SZ, D9, B2M1, D7, Q2F1, D5]; 

V3 = BeamLine[D5, Q3F1, D7, B3M1, D9, SZ, SZ, D11, Q3D1, D13, SX, SX, D15, Q3F2, D17, Q3D2, D19, B3M2, D21, SX, SX, D23, Q3D3, D25, Q3F3, D27];

V4 = BeamLine[D27, Q4F3, D25, Q4D3, D23, SX, SX, D21, B4M2, D19, Q4D2, D17, Q4F2, D15, SX, SX, D13, Q4D1, D11, SZ, SZ, D9, B4M1, D7, Q4F1, D5];

V8VCMD  = BeamLine[SOL204, Dsol204, SOLend, D3, SOL202, Dsol202, SOLend, D101, SOL100, Dsol100, SOLend];
V9VCMD  = BeamLine[SOL100, Dsol100, SOLend, D101, SOL302, Dsol302, SOLend, D3, SOL304, Dsol304, SOLend];
V10VSND = BeamLine[SOL404, Dsol404, SOLend, D3, SOL402, Dsol402, SOLend, D1];

V       = BeamLine[V0VSND, V1, V2, V8VCMD, V9VCMD, V3, V4, V10VSND];
FFS USE V;

READ "functions/functions.sad";
CELL;
FFS["CALC"];

{nx0, ny0}=Twiss[{"NX","NY"},"***"]/(2*Pi)

LINE["LENGTH"] 
LINE["S", -1] 
24.3883 - LINE["S", -1]
 
 
CELL;             ! peridic condition
FIT;              ! set fit point at end of line
NX .1701;          ! set fit condition NX
NY .1699;          ! set fit condition NY
FREE Q*;          ! set Q* (in this case QF and QD) as the matching
                     ! variable
GO;               ! start matching
 
Element["K1", "Q1F1"]
Element["K1", "Q2F1"]
{nx0, ny0}=Twiss[{"NX","NY"},"***"]/(2*Pi)

ABORT 
!==================== final dynapic aperture calc =================================================
 DP0=0.0; EMIT;
 switch = 0;
 Nturns = 2000;                     ! number of turns in tracking
 NL     = 150;                      ! number of "rays" in x-y plane scan
 DA     = {};
                                    ! zadadim ellips fizicheskoy apertury, kak mera dlya shaga i DA. achtung! dlya raznyx de/e ellipsy raznye
 ax     = 4 * 0.05;                 ! po x
 ay     = 4 * 0.02;                 ! po y.

For[j = 0, j <= NL, j++,            ! cycle of "rays" in x-y plane

      axy  = ax * ay / Sqrt[(ax*Sin[2*Pi*j/NL])^2 + (ay*Cos[2*Pi*j/NL])^2];   ! the scale is a ray, cutted by scaling ellipse
      step = axy;                   ! initial step along the ray, it will decrease automatically near DA border
      vec0 = {{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0.0, 0.0}};    ! here - dp/p can be changed

      While[1 > 0,                  ! cycle of moving along the "ray"
          vecBT = vec0 + {Cos[2*Pi*j/NL]*{step,step}, {0,0}, Sin[2*Pi*j/NL]*{step,step}, {0,0}, {0,0}, {0,0}}; ! vec Before Track
          vec   = TrackParticles[{1, {vecBT[[1]],vecBT[[2]],vecBT[[3]],vecBT[[4]],vecBT[[5]],vecBT[[6]],{1,1}} }, 1, 1, Nturns+1][[2]] ;
          Print[vec];
                                    ! ratio of final radius-vector to cutted by scaling vector
          If[(vec[[7,1]] == 0) || (vec[[7,2]] == 0) ||

             (Sqrt[ (vec[[1,1]]/ax)^2 + (vec[[3,1]]/ay)^2 ] > 10) ||
             (Sqrt[ (vec[[1,2]]/ax)^2 + (vec[[3,2]]/ay)^2 ] > 10),

                switch = 1;
          ];
          If[switch == 0,           ! if stable, step is done along the ray
              vec0 = vecBT;
              Print[vec0[[1,1]], " ", vec0[[3,1]] ];
          ];
          If[switch == 1,           ! if unstable, we will try smaller step
              step   = step/2;
              switch = 0;
          ];
          If[step < 0.01*axy,       ! if step is small enough, coordinates printed into file
              DA     = Append[DA, {vec0[[1,1]], vec0[[3,1]]} ];
              switch = 0;
              Break[];
          ];
      ];
];

Print[DA];

!---PLOT---
lp = ListPlot[DA, FrameLabel->{"x,[m]","y,[m]"}, PlotJoined->True, PlotRange->{{-0.15,0.15},{-0.06,0.06}}, PlotLabel->"VEPP with wrong nus"];

ellpl = Plot[{Ellipse[x, 0.05, 0.02], -Ellipse[x, 0.05, 0.02]}, {x, -0.05, 0.05}];
Show[lp, ellpl];
Update[];
TkWait[];
 
 
ABORT
